# README #

The Star Citizen Alternative Patch Downloader implemented in Python.

### What is this repository for? ###

* This tool will let you download the Star Citizen client from any machine with an internet connection, enough disk space and Python3 installed.
* 1.0

### How do I get set up? ###

* Either clone the repository or download the python file and run it with python3 and you're good to go.
* Currently there is no configuration needed. As I expand my knowledge of python I will aim to add arguments for setting a custom download directory, or even specifying a release to download directly from the command line
* Dependencies: Python 3

### How to use this tool ###

1. Download and extract Star_Citizen_Patch_Downloader.py from the repository
2. Run the tool
3. When prompted select which release you would like to download. (Usually the option will be either Public or Test) - This prompt is case sensitive, I will make it case insensitive when I can.
4. The client will be downloader to a Star_Citizen_Client folder in the same location as the python script. Inside this folder you will need to select the build number that matches the version you downloaded (usually the largest number)
5. Copy the contents of this folder to your Star Citizen Install location (C:\Program Files\Cloud Imperium Games\Star Citizen\Test by default)
6. Start your patcher, switch to the relevant release and verify your files.
7. Launch the game and play.

### Known Issues ###

* If you kill the process through any means other than Ctrl+C it will leave the partially completed file behind and not resume it. I am investigating catching the signals sent to kill tasks, but haven't figured this out yet. (If you know, then please let me know and we'll get it sorted)
* I probably haven't caught all of the exceptions that can be thrown, if you find one please raise an issue on the issues page so that I can try to fix the problem.

### Who do I talk to? ###

* Currently the only contributor to this project is NimmoG, but if you want to chip in, please contact me and I'll get you set up.