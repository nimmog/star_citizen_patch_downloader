#!/usr/bin/env python

# Star Citizen Alternative Patch Downloader
# Cloud Imperium Games own all Star Citizen related content and assets.
__author__ = 'Graeme Nimmo'
__copyright__ = 'Copyright (C) 2016 Graeme Nimmo'
__license__ = 'GNU GPL'
__version__ = '3'
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

 #You should have received a copy of the GNU General Public License
 #along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Libraries to allow us to download files
import urllib.request
import shutil

# Required to handle locating files
import os

# Random library used to select a random webseed
import random

# Need this to handle user killing process in the middle of a transfer
import sys

# Import json to read in downloaded file manifest
import json

def fetch_universes():
    file_location = "launcher_info.txt"
    universes = {}
    universe_properties = {}

    #Download launcher info file and trap connection problem exception
    try:
        with urllib.request.urlopen('http://manifest.robertsspaceindustries.com/Launcher/_LauncherInfo') as response, open(file_location,'wb') as out_file:
            shutil.copyfileobj(response, out_file)
    except urllib.error.URLError:
        print('Cannot download file, are you connected to the internet?')
        return None

    # Open the launcher info file and find the names of the releases described
    universe_file = open(file_location,"r")
    universe_list = universe_file.readline().split('=')[1].split(",")

    # Get the list of properties described in launcher info file
    universe_lines = list(universe_file)
    for line in range(len(universe_lines)):
        current = universe_lines[line].split('=')
        if '_fileIndex' in current[0]:
            try:
                universe_properties[current[0][:-1]] = current[1][1:-1]
            except IndexError:
                print('There was an error trying to get the list of universes, what follows is some debugging information that you can send to the writer if this error occurs repeatedly:')
                print(current)

    # Add universe names and file indexes to a dictionary for use elewhere 
    for item in range(len(universe_list) -1):
        universes[universe_list[item][1:]] = universe_properties[universe_list[item][1:] + '_fileIndex']
        
    universe_list[len(universe_list) -1] = universe_list[len(universe_list) -1][1:-1]
    
    universes[universe_list[len(universe_list) -1]] = universe_properties[universe_list[len(universe_list) -1] + '_fileIndex']

    universe_file.close()
    os.remove(file_location)
    
    # Return list of found universes
    return universes
    
def prepare_file_list(file_index_url):
  
    # Download the file manifest
    try:
        with urllib.request.urlopen(file_index_url) as response:
            file_contents = json.loads(response.read().decode())
                
    except urllib.error.URLError:
        print('Cannot download file, are you connected to the internet?')
        return None
      
    return file_contents['file_list'],file_contents['key_prefix'],file_contents['webseed_urls']

def download_files(file_list, url_prefix, webseed_urls, download_path):
    for item in range(len(file_list)):
        url = webseed_urls[random.randint(0,len(webseed_urls) -1)] + '/' + url_prefix + '/' + file_list[item]
        relative_path = url_prefix.split('/')[2]
        
        # Generate the download path. os.path.join creates an OS independent path (*nix uses / between directories, but Windows uses \)
        # *file_list[item].split('/') will convert the string with the path for the current file into a list to be pieced
        # back together by os.path.join.
        download_location = os.path.join(download_path , relative_path , *file_list[item].split('/'))

        # Create the directories if they don't exist
        
        os.makedirs(os.path.dirname(download_location), exist_ok=True)
        
        # Try to download the files
        print('Downloading file (' + str(item + 1) + ' of ' + str(len(file_list)) + '): ' + file_list[item] )
        if not os.path.isfile(download_location):
            try:
                with urllib.request.urlopen(url) as response, open(download_location,'wb') as out_file:
                    shutil.copyfileobj(response, out_file)
            except urllib.error.URLError:
                print('Something went wrong whilst downloading, try again.')
            except KeyboardInterrupt:
                print('Interrupted by user')
                try:
                    os.remove(download_location)
                except FileNotFoundError:
                    pass
                sys.exit()
    
# Main section of the program
if __name__ == "__main__":
    # Print some kind of boilerplate welcome message
    print('Welcome to the Star Citizen Alternative Patch Downloader written by NimmoG')

    # Get list of Versions
    print('Fetching list of current releases')
    universe_list = fetch_universes()

    # Select Version
    print(str(len(universe_list.keys())) + ' releases were found.')
    print('-------------------------')
    print('Releases Found:')
    for item in range(len(universe_list.keys())):
        print(list(universe_list.keys())[item])

    selection = input('Please enter the client you would like to download from the list above:').title()
    while (selection not in universe_list):
        print('Please enter an item from the list above.')
        selection = input('Please enter the client you would like to download from the list above:').title()

    # Fetch filelist for this version
    file_list, url_prefix, webseed_urls = prepare_file_list(universe_list[selection])

    #Select download location
    download_path = os.path.join(os.getcwd() , 'Star_Citizen_Client')
    print('Default Download path has been set to:\n' + download_path)

    # Download files
    download_files(file_list, url_prefix, webseed_urls, download_path)
